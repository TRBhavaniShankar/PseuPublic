# The Pseu programming language

## Overview

The purpose of the pseu language is to provide a compact notation for describing algorithms at a high level of abstraction.

For example types such as sets, sequences, tuples, and maps are built-in data types.

## Some examples

The examples may use syntax that is beyond the core. The extra syntax used is described in the document "syntaticSugarAndOtherExtensions".

This procedure will find all permutations of a set of integers

```
  fun perms( S : Set[Int] ) : Set[Seq[Int]]
      var P : Set[Seq[Int]]
      if #S = 0 then
          P := { [ ] }
      else
          val a := s.any()
          var P0 : Set[Seq[Int]]
          P0 := perms( S-{a} )
          P := { }
          for seq <- P0 do
               for i <- [0 ,.. #S] do
                   P := P union { seq[0,..i] ^ a ^ seq[i,..] }
     return P
 end perms
```

Here is a blow by blow explanation of the program. 

The first line declares that `perms` is a procedure. It has an input parameter `S` of type `Set[Int]` (i.e. `S` represents a set of integers). 

The declaration `var P : Set[Seq[Int]]` declares a variable `P` of type `Set[Seq[Int]]`. Which means `P` represents a set of sequences of integers.

The expression `#S = 0` is true if and only if the size of `S` is 0.  When this true `p` is assigned a set containing only the empty sequence `[ ]`.

After the `else`, there is a declaration of a local constant `a`, which is initialized with an arbitrary member of set `S`. Variable `P0` is declared to have the same type as `P`; it is not initialized. The next line is a recursive call to procedure `perms`.  The argument `S-{a}` is the set of all think in `S` other than `a`.

Next, `P` is assigned the empty set `{ }`. 
The following lines define two nested for loops that add values to set `P`. 

The second last line indicates that the result of the function is the value of variable `P`.

The final line marks the end of the function definition.

It should be noted that sets and sequences in Pseu are mathematical in nature. That is, they are immutable values. Our variable `P` refers to different values at different times rather than always referring to one object whose value changes.

## Some general remarks on syntax

The syntax of the language is mostly conventional.  It uses `:=` for assignment and initialization and `=` for equality.  It is block structured.   It does not require, but allows, `;` after each command or declaration.

There are some unusual rules about indentation, similar to, but different from, Python.  These are beyond the core and are described in the document "syntaticSugarAndOtherExtensions".

## Data types and data values

The language has at least the following data types and values

* `Bool` the boolean values `true` and `false`.
* `Int` integers of any size.
* `String` immutable sequences of characters of any length
* `Set[T]` immutable finite sets containing only items of type `T`. E.g. `Set[Set[Int]]`
* `Seq[T]` immutable finite sequences containing only items of type `T`. E.g. `Seq[Bool]`.
*  `T * U` pairs of values. The first value is from `T`, the second is from `U`.  Similarly `T * U * V` is the type of triples of values and so on for longer tuples.
*  `Unit` is a type that only has one value, namely the empty tuple.
* `T -> U` functions from `T` to `U`.
* `Any`  All values have type `Any`
* `None` No values have type `None`.

Note there are no length 1 tuples. 

Implementation note: It is important that the set of types and the set of values be easily expandable.

## Constants 

The keywords `true` and `false` .

Integer constants are sequences of 1 or more decimal digits. Underscores can be used to ease readability. E.g., `0`, `123`, `123_456_789_012_345`.

String constants are enclosed by double quotes. E.g. `""`, `"abc"`, ... .

## Constructors

We can construct a set from a sequence of expression using the syntax. `{X, Y, Z}` where `X`, `Y`, and `Z` are expressions.

We can construct a sequence from a sequence of expression using the syntax. `[X, Y, Z]` where `X`, `Y`, and `Z` are expressions.

Pairs, triples, etc. are made using parentheses and. E.g. `(X,Y,Z)` makes a triple. `()` makes the 0-tuple. There are no 1-tuples in the language, so `(X)` just means `X`.

Functions are made using lambda expressions.  An example of a lambda expression is `fun( x : Int ) return x+1 end fun`


## Operations

On each value, there is a set of operations determined by the type. E.g. values of type `Int` type has operations `+`, `-`, `-`, `*`, `mod`, `div`, `=` and others. The type `Set[T]` has operations `union`, `intersection`, `-`,  `=` and others.

An operation is invoked by sending a message to a value.  The general syntax for message sending is `E.m(F)` where `E` is an expression and `F` is an expression.  The semantics is that `E` and all the expressions in `F` are evaluated to get values $e$ and $f$. A message $m(f)$ is sent to value $e$. 

For example, if variable `a` has value $123$ and variable `b` has value $654$, then the expression `a.+(b)` means send the message $+(654)$ to value $123$.

For some operations, there are other syntaxes outside the core for message sending. For example the expression `a.+(b)` can also be written as `a + b`.  This is purely a matter of syntax. 

Messages can have more than one or less than one argument.  These cases are equivalent to sending a single argument. Again, this is a trivial syntactic matter.  An expression `E.m()` is equivalent to the expression `E.m(())`; that is, the argument will be the empty tuple.  An expression `E.m(F,G,H)` is equivalent to an expression `E.m((F,G,H))`; so the single argument value will be a triple.

Message sending actually has two steps.

* The first step is look-up. This step looks up the name in the recipient and results in a function value.  For example the result of `123.+` is a function value.
* The second step is application. This step applies the resulting function to an argument.

These steps can actually be separated. The following code should work

~~~
     val f : Int -> Int := 123.+ ;
     print( f(321) )
~~~

The first line gives the name `f` to the result of the lookup. The second line applies that function.

## Type checking

[For the purposes of the 9874 project, type checking is only done at runtime. This includes checking the number of arguments passed to a function call. For example, the expression `12 + "abc"` will result in no error at compile time, but will result in an error at run time, since the `+` operation for integers is not prepared to accept a string as the argument.  Similarly an expression `12 .+(34, 56)` would not be an error at compile time, but will result in an error at run time. In this case the argument be a pair and the + operation for integer values is not defined when the argument is a pair.]

The following type checks need to be made at run-time. 

* When a variable (val or var) is assigned, the value must be in the type of the variable. Otherwise, it is a run-time error.
* When a user defined function with one parameter is applied, the value passed as argument must be the same type as that parameter.
* When a user defined function with 0 parameters is applied, the argument value must be the empty tuple.
* When a user defined function with 2 or more parameters is applied, the argument must be a tuple of the same length and each item of that tuple must be in the type of the corresponding parameter
* Similar checks for built-in functions.
* In a lookup expression, such as `x.y`, the name `y` must be mapped to something for the value `x`. E.g., `123.+` is ok since `+` is defined for all integers.  But `123.foobar` should probably result in a run-time error.

[As a bonus goal, you can add compile time type checking.]

## Core syntax

The following describes the core syntax of the language. Constructs that use the full syntax can be rewritten to use the core syntax, so we will start by implementing the core and add elements of the full syntax as needed.

The syntax in this section is given using extended CFG notation.  Terminals of the language are written in double quotes or as lower case words.  The meaning of these words is discussed in the Section called Lexical rules. Nonterminals are written as words.   The right hand side of each production is written with the following conventions:
 
 >  X Y
  
 means X followed by Y;
 
 > X | Y
 
 means either X or Y;
 
 > X?
 
 means an optional X;
 
 > X*
 
 means zero or more X;
 
 > X+
 
means one or more X.  Unquoted parentheses are used for grouping.
 
~~~
 Block --> 
         | Command (";")? Block
         | Declaration (";")? Block
 
 Command -->  LHSs ":=" Exps
           |  "if" Exp "then" Block "else" Block "end" "if"
           |  "while" Exp "do" Block "end" "while"
           |  "for" id "<-" Exp "do" Block "end" "for"
           |  "return" Exp
           | ExpCommand
   
ExpCommand --> id PostFixOp
            |  ExpCommand PostFixOp

PostfixOp --> "." Name | Arg
 
LHSs --> LHS ("," LHS)*
 
Exps --> Exp  ("," Exp)*
 
LHS --> id
 
Declaration --> "var" id ":" Type (":=" Exp)?
              | "val" id (":" Type)? ":=" Exp

Name --> id | Op

Exp --> SimpleExp
     |  Exp PostfixOp

SimpleExp --> "true"
         | "false"
         | stringLiteral
         | intLiteral
         | id
         | "{" (Exp ("," Exp)* )? "}"
         | "[" (Exp ("," Exp)* )? "]"
         | "fun" "(" Parameters  ")" (":" Type) "do" Block "end" "fun"
         | ParenthizedExp
 
ParenthizedExp --> "(" (Exp ("," Exp)* )? ")"
         
Arg -->  ParenthizedExp 

Op --> "*"
      | "mod"
      | "div"
      | "union" | "intersection"
      | "and" | "or" | "not" | "implies"
      | otherOp
      | latexOp

Type --> ProductType 
         | Type "->" ProductType

ProductType --> SimpleType ("*" SimpleType)*

SimpleType --> id
             | id "[" Types "]"
             | "(" Type ")"

Types --> Type ("," Type)*

Parameters --> (Parameter ("," Parameter)*)?

Parameter --> id ":" Type

~~~

## Meaning

### Blocks
 
 A block of commands and declarations is executed from left to right.  All the variables (whether val or var) are scoped to the block.  However, it is not allowed (by a run time check) for a variable to be read before it has been declared and initialized.  Thus
 
~~~
    val f := fun () : Int do return x end do
    val x : Int := 42
    print( f() )
~~~

should print 42.

Code in inner blocks can access variables declared in outer blocks.  For example

~~~
   val x : Int := 42
   val y : Int := 10
   while y./= ( 0 ) do
       print(x)
       print(y)
   end
~~~

Should compile and run just fine.

Variable resolution should be done at compile time. Thus the program

~~~
    print(x)
~~~

should cause a compile time error, since `x` is not declared.  (The reference to the `print` variable is no problem, since `print` is a built-in variable which is implicitly declared at block level 0.)

### Declarations

A declaration

~~~
   val x : T := E
~~~

declares a variable `x`, having type `T`, initialized by expression `E`.  Since the variable is declared as a `val`, it can't be assigned to and the compiler should report as errors any assignments to `val` variables.



~~~
   val x := E
~~~

declares a variable `x`initialized by expression `E`.  Since the variable is declared as a `val`, it can't be assigned to and the compiler should report as errors any assignments to `val` variables.

A declaration 

~~~
   var x : T := E
~~~

is similar, but assignments to `x` are allowed.

~~~
   var x : T 
~~~

is similar, except that the variable is initially uninitialized. Any fetch from the variable before it is assigned a value is a run-time error.

### Commands

#### Assignment commands

An assignment

~~~
   x := E
~~~

changes the value of `x` to the value of `E`.

The right hand side can contain 2 or more expressions separated by commas.  The parser treats these as if they were a tuple.

~~~
   x := E, F, G
~~~

abbreviates

~~~
   x := (E,F,G)
~~~

Both assign `x` a value that is a triple (i.e., a 3-tuple).

The left hand side can consist of 2 or more variables separated by commas.

~~~
   x, y, z := E
~~~

In this case, the expression should evaluate to a triple --- this should be checked at run time, not compile time.  If `E` does evaluate to a triple, then the three components are assigned (from left to right) to the three variables.

There is a run-time check that variables can only be assigned values of the appropriate type.

For example:

~~~
    var x : Int := "123" // Run-time error
~~~

and 

~~~
    var y : Seq[Int]
    y := 123 // Run-time error
~~~

#### If commands

~~~
   if Exp then Block0 else Block1 end if
~~~

means what you would expect.

There should be a run-time check that the expression evaluates to a boolean value (either true or false).

#### While commands

~~~
   while Exp do Block0 end while
~~~

means what you would expect, again there is a requirement that the expression is boolean.

#### For command

A for command

~~~
   for x <- E do Block end for
~~~

is exactly equivalent to the following code

~~~
    begin
       val it := (E).iterator()
       while it.notEmpty() do
          val x := it.next() 
          begin
             Block
          end
        end while
    end
~~~

where `begin` and `end` mark the start and end of internal blocks and where `it` is actually a variable name that is guaranteed not to clash with any other variable in the program. This can be accomplished by actually using a name like `#7548`. This works because the language does allow `#` in a variable name. The `begin` and `end` brackets aren't actually a part of the language, but they could be added easily.

#### Return command

`return E` returns from a function invocation; the value of the invocation will be the value of `E`.

#### Expression commands

A limited set of expressions are allowed as commands.  The main purpose of expression commands is to allow functions to be called purely for their side effects. 

### Expressions

#### Lookup

A lookup looks like `E.n` where E is an expression and `n` is a name.  The lookup is resolved at run-time.  For each value, there is a set of names that can be looked up.  If `n` is in that set, the lookup succeeds and the result is some value.  Usually this value is a built in function value. For example consider the lookup `123.+`. Here is what will likely happen at run time

*  Since the value is an integer we go to the run time representation of the `Int` type.
*  The machine looks up the name "+" in that type. This yields some kind of object representing the addition.  I'll call this the "method object".
*  The machine sends a message to the method object with the value (of `123`). The result is a (built-in) function value.
*  That is the result of the lookup expression.

Now what about `123.foobarbang`?

*  Since the value is an integer we go to the run time representation of the `Int` type.
*  The machine looks up the name "foobarbang" in that type.
*  Nothing is found, so the machine reports a run-time error.

If we had compile-time checking, then the compile-time type of `E` would allow us to report at compile-time lookups that might fail.  For those sure to succeed we would get a type for the function returned.  E.g. the type of the expression `123.+` would be `Int -> Int`.

#### Application

An application expression `E(F)` means the value of `E` is to be applied to the value of `F`.  

Again, this requires run-time type checking.  E.g. `123(456)` would compile fine, but should result in an error at run-time since integers don't support application.

The following values support application.

* Sequences.
* Built-in function values.
* Defined function values (also known as "closures").

For example

~~~
   val s := [10,11,12]
   print( s(2) )
~~~

should print 12.  The value of `s` is a sequence. It is applied to 2 and the result is 12.  (Sequences are indexed from 0.)  The value of variable `print` is a built-in function. It is applied to 12. The effect is that 12 is printed to the output channel. (Changing the 2 to any value other than 0, 1, or 2 would result in a run-time error.)

Consider 

~~~
   val x := 123
   print( x.+(765) )
~~~

The result of the lookup operation `x.+` is a built-in function value. This is applied to 765 and the result is 888.

Consider 

~~~
   val f := fun (y:Int, z:Int) : Int
                if x.<(y) then return x
                else return y
                end if
            end fun
              
   print( f(123,456) )
~~~

In this case the value of `f` is a defined function value (or closure).  The argument is a pair (or 2-tuple) (123,456). 

Application in this case works like this.

* A new memory frame is created. A memory frame is the run-time representation of a block. This frame has two locations, one for x and one for y.
* The items of the argument tuple are divided up between the parameters of the function. The two locations of the new frame are initialized with 123 and 456 respectively.
* The body of the function is executed.
* On the return command being executed, the function invocation is returned from and the value of the application is the same as the value of the expression in the return command.

### TODO

More will be added at this point to discuss the meanings of the other kinds of expressions.

## Lexing

[Note for 9874: You don't need to implement any of this, but it's good to know for testing.]

Before lexing happens, Java-style unicode escapes are processed.   This lets you use characters not in ascii while keeping the file in ascii.
For example the input `val \u0061 : Int := 1` is treated as if the input were `val a : Int := 1`.  This is most useful inside string literals.

Comments are as in C++, starting with two slashes and ending with the end of the line.

Spaces, carriage returns, and newlines are skipped, but can be useful to separate tokens.

Identifiers (id) must start with a letter and may contain letters, digits, and underscores. Identifiers must end with a letter or digit. Reserved words, such as "if" and "fun" are not considered identifiers. Case matters, so "IF" is an identifier and "A" is a different identifier from "a".

[Currently only the 52 upper and lowercase ascii letters are allowed and only the 10 ordinary decimal digits.]

String literals (stringLiteral) are the same as in Java, so double quotes must be used and escape sequences such as \n, \r, \\, and \" may be used.

Integer literals (intLiteral) are made up of decimal digits. Underscores may be used internally, but not at the start or end.

Any nonempty sequence of characters from the set { `~`, `!`,  `@`, `#`,`$`, `%`, `^`, `&`,  `*`,`-`, `_`, `+`, `=`, `|`, `/`, `<`, `>`, `?`, `\` } is considered an other operator (otherOp), unless it is used for something else in the grammar.  So `+` is an other operator as is `/\` and `_<`.  However `->` is not, since it has other uses in the grammar.  Essentially `->` is a reserved operator, as are `<-`, `:=` and `*`.

LaTeX operators (latexOp) consist of a backslash followed by one or more letters.  E.g. `\le`. These are useful for expressing operators that aren't in the ascii character set without having to leave the ascii character set.

Lexing follows the "maximal munch" rule meaning that as the input is processed from left to right, the characters are packed into tokens in a greedy fashion; i.e. as many characters are stuck together to make a token as possible before a token is produced.  For example

* `\le123` is 2 tokens (`\le` and `123`) since a LaTeX operator can not contain digits
* `abc_123` is one token --- an identifier.
* `abc _123` is three tokens (`abc`, `_` and `123`). 
* `a+-b` is three tokens (`a`, `+-`, and `b`)
* `a+ -b` is four tokens (`a`, `+`, `-`, and `b`)




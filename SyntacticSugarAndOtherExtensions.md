# Syntactic sugar and other extensions

This document contains ideas on how to extend the syntax from the core language to the full language.

Some of these ideas will be implemented by me before the end of 9874.  Some are only half baked and need some thought and experimentation.

In any case, 9874 students won't have to implement any of these ideas, they should stick to implementing the core language.  So they shouldn't spend a lot of time (if any) reading this document.

This section contains suggestions for how the syntax can be extended beyond the core to make the language more flexible and appealing to use.

## Purely syntactic changes

Many of these additions are purely syntactic and involve little or no additional abstract syntax.

### Operator syntax

[I do plan to implement this for 9874.]

An expression `E + F` abbreviates `(E).+(F)`.  An expression `- E` abbreviates `(E).-()`.  The default precedence of binary operators is given by

| operator | precedence |
------------------|---------------------|
| implies    | 100            |
| or            |  200            |
| and            |  300            |
| =              | 400            |
| <            |  400            |
| >           |  400            |
| >_           |  400            |
| _<           |  400            |
| \le          |  400            |
| \ge         |  400           |
| union     |  500   |
| \cup     |  500   |
| +            |  500   |
| -            |  500   |
| *      |  600   |
| intersection     |  600   |
| \cap | 600 |
| /     |  600   |
| div     |  600   |
| mod     |  600   |
| ^      |  700   |
| other operators | 1000 |

The other operators come in two flavours.

* Any string of one or more characters from the set { `~`, `!`,  `@`, `#`,`$`, `%`, `^`, `&`,  `*`,`-`, `_`, `+`, `=`, `|`, `/`, `<`, `>`, `?`, `\` } otherwise used in the grammar.

* A backslash `\` followed by one or more letters is also considered an operator. This is useful for translating Pseu into LaTeX.

Binary operators at the same precedence level always associate to the left, so for example `a ^ b ^ c` is the same as `(a ^ b) ^ c` and ultimately the same as `(a.^(b)).^(c)`.

Unary operators have higher precedence than binary operators, but lower precedence than send operations and other postfix operations. So
`- a + - b.f()` is the same as `a.-() + b.f().-()`.

The unary operators any binary operator may also be used as unary operator and also the keyword "not".

### Ranges

Make `,..` and `,..,` be tokens.

Define `[x,..y]` to mean `x.upTo(y)`; define `[x,..,y]` to mean `x.through(y)`; define `{x,..y}` to mean `x.upTo(y).toSet()`; and define `{x,..,y}` to mean `x.through(y).toSet()`.

### Range application

Define `s(x,..y)` to mean `s.subSeq(x, y)`.  If `s` is a sequence and `x` and `y` are integers.

Similarly for `,..,`.


### <a id="brackets"></a> Brackets

Allow square brackets for application.  Some people might prefer to use `s[5]` to `s(5)`. 

This conflicts somewhat with the idea below of allowing the parentheses to be dropped. Then does `s[5]` really mean `s(5)` or `s([5])`.  Similarly does `s[1,2,3]` mean `s(1,2,3)` or `s([1,2,3])`.
With this proposal, we pretty much have to pick the first.  Without this proposal, but with the other proposal, we have to pick the second.

Similarly define `s[x,..y]` to mean `s(x,..y)`.  Note that when `x` equals `y`,  `s[x]` and `s[x,..y]` have different meanings, whereas (if they have any meaning at all) `s([x])` and `s([x,..y])` have the same meaning.  Presumably the meaning of `s([x,..y])` would be the same as `s(x,..y)`.

### Implicit typing

[I do not plan to implement this for 9874.]

~~~
    var x := E
~~~

abbreviates

~~~
    var x : T := E
~~~

where T is the the most specific type for E.

### Function declaration

#### Directly declaring a function or procedure


[I do plan to implement this for 9874.]

A declaration

~~~
	val f := fun (a : t0, b: t1) : u do ... end fun
~~~

can be abbreviated by

~~~
    fun f(a : t0, b: t1) : u do ... end fun
~~~

A declaration

~~~
	val f := fun (a : t0, b: t1) : Unit do ... end fun
~~~

can be abbreviated by

~~~
    proc f(a : t0, b: t1) do ... end proc
~~~



### Short form lambda expressions

[I do plan to implement this for 9874.]


The expression

~~~
   (a: t0, b: t1) -> E
~~~

where E is a simple expression, abbreviates

~~~
fun ( a: t0, b: t1) do return E end fun
~~~

The precedence of the `(...) ->` prefix is the same as unary expression.

### Indentation rules

[I might implement this for 9874.]

The full syntax includes indentation restrictions. In return the various "end" markers can be omitted.

The basic restriction is that each command or declaration in a block must begin at the same column or on the same line. For example

```
      a := 1 ; b := 2
```

can be a block because both commands start on the same line.  And

```
     a := 1 ;
     b := 2 ;
```

can be a block because both commands start at the same column.  But

```
     a := 1 ;
         b := 2 ;
```

can not be a block. Nor can

```
     a := 1 ;
   b := 2 ;
```

Furthermore, the first token of each block must begin at a column number larger than the first token of its containing command or declaration. For example

```
    if a < b then
       a := c
    else  b := c
```

is allowed because the block between `then` and `else` starts at a larger column number than the keyword `if` and likewise the  block that comes after `else`.

Blocks can be optionally ended with `end`-markers.  For example, the we could write

```
    if a < b then
       a := c
    else 
        b := c
    end if
```

### Dropping parentheses from application commands


[I might implement this for 9874.]

For commands, we can allow final the parentheses to be dropped

So the command `print E,F,G` abbreviates `print(E,F,G)` which abbreviates `print.apply(E,F,G)`.

I don't think there is any ambiguity for single or multiple arguments.

If we allow this abbreviation also for 0 argument message sends, there is an ambiguity.  Consider a block

~~~
   b c d
~~~

Should this mean

~~~
   b() ; c(d) 
~~~

or

~~~
   b(c) ; d()
~~~

?

This question is very similar to the question of how to deal with a return that has no expression.  (See below.) It should be answered the same way.

### Dropping parentheses from expressions

[I might implement this for 9874.]

It is harder to allow these parentheses to be dropped from expressions because it creates ambiguity.  E.g. the command

~~~
	a := f x, y
~~~

could mean either

~~~
	a := f(x), y
~~~

or

~~~
	a := f(x, y)
~~~

We could, though, permit the single argument of a function to be given as a simple expression. E.g. `f a` or `g.foo [1,2,3]` or even `f x y`, meaning `f(x)(y)`, which in turn means `(f(x))(y)`. 

I don't think this creates any ambiguity, but it does make parsing a little harder. For example the block

~~~
    x : = f a := b
~~~

becomes a bit harder to parse correctly using a top-down parser.

See also the discussion in section [Brackets](#brackets).

### Syntax rules

Syntax rules that accommodate several of these extensions are as follows

~~~
// Infix operators
Exp --> Exp Op Exp1 | Exp1
// Prefix operators
Exp1 --> (Op | "not") Exp1 | Exp2 
Exp2 --> SimpleExp
Exp2--> Exp2 "." Name
// No parentheses required on arguments.
Exp2--> Exp2  (SimpleExp)?
~~~

To make parsing and reading easier, we could have a rule that in the cases of the optional simple expressions, if the next token could be the start of a simple expression and (is on the same line, or (is on another line, but it indented more than the current block level)), then the parser should attempt to parse the SimpleExp should be parsed, even if this leads to an error.  That would mean that 

~~~
    x : = f a := b
~~~

and

~~~
    x : = x.f a := b
~~~

lead to error, but that

~~~
    x : = f ; a := b
    x : = x.f ; a := b
    x : = f
    a := b
    x : = x.f
    a := b
~~~

is allowed, as is

~~~
    x : = f a
    x : = x.f a 
    x : = f
           a
    x : = x.f
            a 
~~~

Inside parentheses, brackets, etc, the rule is not needed. We could trick the parser by using a negative block indentation.

###Arbitrary expressions as commands


[I might implement this for 9874.]

A similar loosening of syntax is to allow an arbitrary expression as a command.  This really does create an ambiguity. E.g.

~~~
   a := f (a.b()).c()
~~~

could mean

~~~
   a := (f (a.b()).c())
~~~

or it could mean

~~~
   a := f  ;  (a.b()).c()
~~~

One way to deal with this is to give the first interpretation preference when the first token of the function argument is on the same line and to the second interpretation when the potential argument starts on another line.


(Or we could insist that the second interpretation only holds when the first token of the potential argument is not only on another line, but also that it is at or to the left of the current block indentation level.  This would allow

~~~
       a := f(a + b)
             (c + d)
~~~

Here the expression `f(a+b)` presumably returns a function that is then applied to the result of `(c+d)`.)

### return without an expression

It is tempting to allow `return` as a command that abbreviates `return ()`.

This creates an ambiguity.  E.g.

~~~~
    return a.b()
~~~~

could mean either

~~~~
    return ; a.b()
~~~~

or

~~~~
    return( a.b() )
~~~~

One way out of this ambiguity is to prefer the second interpretation when the potential result is on the same line and the first with it is on another line.

(Or we could insist that in the second case, the argument must both start on another line and start to the left or even with the "return".  This would mean

~~~~
   ...
    return 
        a.b()
   ...
~~~~

is a regular return whereas

~~~
  ...
     return 
  a.b()
  ...
~~~

is an abbreviated return followed by the end of its block and then a command from an outer block.)

## Deeper enhancements

* Compile-time type checking
* User defined classes and interfaces
* Inheritance and overriding
* Function overloading of some kind
* Algebraic data types and pattern matching
* Mutable sets and sequences
* out parameters?
* Pre and post conditions
* Automated verification
